# README #
Documentation

### Android library for image caching and json caching prepared exclusively for MindValley challenge ###


### How do I get set up? ###

* Initialize using key "mind"
eg. 
        Pinload.initialize(MainActivity.this,"mind");

* Methods :
* 1. For loading and caching images:
 * Pinload.imageLoader(Context,URL,imageView target);
* 2. For canceling an image load :
* Pinload.cancelImageLoad(String urlofImage); 
*//
*where url of image is considered as a tag
*//
*3. Save a json object in cache mem. for offline loading
* Pinload.saveJsonObject(Context, jsonObject, tag);
* Note : jsonObject here is a String. You can easily convert a JSONObject to a String using following method and then calling the above method:
 *          String serializable = PinloadJSON.convertToSerializableObject(person)
  *        Pinload.saveJsonObject(MainActivity.this, serializable,""+i+"tag");

*4. Retrieve the jsonObject in format of Object :
*Pinload.retrieveJsonObject(String tag);


### Contribution guidelines ###

* Google Volley
Used and modified volley library to tweak up some image fetching and displaying algo.
* Apache http, mime libraries
For handling HTTP requests
* Google gson library
For fast conversion of JSON to String

### What's so fascinating about this library? ###

* Faster than image caching library Picasso (Developed by Square) in terms of loading images from web
* Inbuilt premium fade in animation. Working on implementing more animations in the future.

### This library is developed solely by Shantanu Johri. It's a part of MindValley's recruitment challenge.