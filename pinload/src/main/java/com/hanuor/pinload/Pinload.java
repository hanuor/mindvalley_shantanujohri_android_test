package com.hanuor.pinload;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.hanuor.pinload.handler.VolleyHelper;
import com.hanuor.pinload.toolbox.ImageLoader;
import com.hanuor.pinload.verifiers.TemporaryDB;
import com.hanuor.pinload.verifiers.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Shantanu Johri on 31-07-2016.
 */
public class Pinload{
    private static Context ctx;
    private static   TemporaryDB temporaryDB = new TemporaryDB();

    private static ImageLoader imageLoader;
    public static void initialize(Context context, String key){
        ctx = context;
        VolleyHelper.init(context);
         imageLoader = VolleyHelper.getImageLoader();

        if(context== null || key == null) {
            Utils.throwExceptionIfNullOrBlank(context, "context");
            Utils.throwExceptionIfNullOrBlank(key, "libraryKey");
        }
        else{
            temporaryDB.setKeygen(key);
        }
    }
    public static void saveJsonObject(Context context, String jsonObject,String tag) {
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput(tag, Context.MODE_PRIVATE);

        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(jsonObject);
        oos.flush();
        oos.close();

        fos.close();

            Log.d("CheckJson","yes");
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            Log.d("CheckJson","no");
        } catch (IOException e) {
            e.printStackTrace();

            Log.d("CheckJson","no");
        }

    }
    public static Object retrieveJsonObject(String tag){

        try {
            FileInputStream fis = ctx.openFileInput(tag);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object object = ois.readObject();
            fis.close();
            return object;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void imageLoader(Context context, String URL, ImageView target) {
        ctx = context;
        Log.e("LRU","loader");
        if(temporaryDB.getKeygen().equalsIgnoreCase("mind")){

            imageLoader.get(URL,ImageLoader.getImageListener(target, R.drawable.more, R.drawable.more));


        }else{
            Utils.throwExceptionIfKeyDNM(temporaryDB.getKeygen().toString(), "libraykey");
        }
     }
    public static void cancelImageLoad(String urlofImage){
        imageLoader.cancelRequestfromQueue(urlofImage);

    }


}
